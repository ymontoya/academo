import React from 'react';
import '../Input/Input.scss';


const InputTextArea = ( { value, title, on, id, type, name, autoComplete, error, errorMessage, required } ) => (

    <div className="input__content">
        <label>
            {title}
        </label>
        <textarea id={id} 
            type={type}
            name={name}
            value={value} 
            onChange={on}
            autoComplete={autoComplete}
            className={ 
                ` ${ error ?
                    'invalid'
                    :
                    ''
                }
                `
            }
            required={ required ? true : false }
        />
        { error ?
            <span className="alert" >
                {errorMessage}
            </span>
            : ''    
        }
    </div>
);

export default InputTextArea;