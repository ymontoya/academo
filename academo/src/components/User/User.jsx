
import React, { useState } from 'react';
import './User.scss';

import { useHistory } from "react-router-dom";

import userImg from '../../assets/img/user.png';

const User = () => {

    // botón cerrar cesión
    let [ hiddenButton, setHiddenButton ] = useState(false)

    /// navegar
    let history = useHistory();

    // Cerrar cesion
    const closeSesion = () => {
        history.push('/');
    }

    return( 
        <div className="user">
            <div className="user__img">
                <img src={userImg} alt="Usuario"/>
            </div>
            <div className="user__info">
                <span>
                    Usuario
                </span>
                <button className="user__options" onClick={ e => setHiddenButton(!hiddenButton)}>
                    Opciones 
                    <i className="material-icons">expand_more</i>
                </button>
                <button 
                    className={`user__close-sesion ${ hiddenButton ? 'hidden' : '' }`} 
                    onClick={ e => closeSesion() }
                >
                    <i className="material-icons">power_settings_new</i>
                    Cerrar sesión
                </button>
            </div>
        </div>
    );
}

export default User;