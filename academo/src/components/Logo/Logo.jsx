import React from 'react';
import './Logo.scss';

const Logo = ( { left } ) => (
    <span className={`logo ${left ? 'left' : '' }`} >
        <i className="material-icons">play_arrow</i>
        <h1>
            Academo
        </h1>
    </span>
);

export default Logo;