import React, { useState } from 'react';
import './Video.scss';
import ReactPlayer from 'react-player'

// complementos 
import imgVideo from '../../assets/img/imagen-video.png';
import InpuTextArea from '../../components/InputTextArea/InputTextArea';
import Button from '../../components/buttons/Button';
import User from '../../components/User/User';
import Logo from '../../components/Logo/Logo';

const Video = () => {

    // usuario
    let user = 'Yeison';

    // lista de comenyarios
    let [ comments, setComments ] = useState([{
        'usuario': 'Usuario 1',
        'comment': 'Estes es un comentario'
    },
    {        
        'usuario': 'Usuario 2',
        'comment': 'Estes es un comentario'
    }]);


    //datos cometarios
    const [ newComment, setNewComment ] = useState();
    const handleInputChange = e => {
        setNewComment({
            ...newComment,
            [e.target.name]: e.target.value
        })
    }

    const resetComment = () => {
        setNewComment([])
    }

    let [ noComments, setNoComments ] = useState(false);



    // agregar comentario
    const handleComment = e => {
        e.preventDefault();
        if( newComment === null || newComment === undefined ) {
            setNoComments(true);
        } else {
            setComments(  [ ...comments, { usuario: user, comment: newComment.comment }]);
            setNoComments(false);
            resetComment();
        }

    }

    // video 
    let [ isVideo, setIsVideo ] = useState(false); 


    return(
        <div className="video">
            <div className="video__video scroll">
                <div className="video__cont">
                    { isVideo ?
                        <ReactPlayer 
                            className="video_element"
                            // src={video}
                            url="https://www.youtube.com/watch?v=wOKYdbaJvGs&t=1023s" 
                            width="100%" 
                            height="550px"
                            type="video/mp4" 
                            autoPlay 
                            controls 
                            preload="auto"
                            controls="controls"
                        />

                        : 
                        <>
                            <img className="back__video" src={imgVideo} alt="Llamado asincrono JS"/>
                            <button className="btn__play" onClick={ e => setIsVideo(!isVideo) }>
                                <i class="material-icons">play_arrow</i>
                            </button>
                        </>
                    }
                </div>
                <div className="video__description">
                    <Logo left />
                    <h1>
                        JavaScript Dynamic imports
                    </h1>
                    <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Neque, explicabo excepturi nam consequuntur unde maxime illum similique perferendis vero adipisci commodi doloremque nesciunt voluptate! Nihil optio odit deleniti porro natus!
                    </p>
                </div>
            </div>
            <div className="video__chat">
                <User />
                <div className="comments scroll">
                    { comments.map((item) => (
                        <div className="comment__cont">
                            <div className="comment__cont--avatar">
                                <i class="material-icons">face</i>
                            </div>
                            <div className="comment__cont__block">
                                <span className="comments__cont--user">
                                    {item.usuario}
                                </span>
                                <span className="comments__cont--text">
                                    {item.comment}
                                </span>
                            </div>
                        </div>
                    ))}
                </div>
                <hr/>
                <form onSubmit={(e) => handleComment(e)} >
                    <InpuTextArea 
                        title="Comentar" 
                        name="comment"
                        id="comment"
                        on={handleInputChange}
                    /> 
                    { noComments ?
                        <span className="alert">
                            Necesita escrinir un comentario.
                        </span> :''
                    }
                    <Button 
                        btn 
                        iconL="speaker_notes" 
                        type="submit" 
                        title="Comentar" 
                        error={noComments}
                        errorMessage="Necesita escrinir un comentario."
                    />
                </form>
            </div>
        </div>
    ); 
}

export default Video;